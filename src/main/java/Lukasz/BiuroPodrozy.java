package Lukasz;


import java.util.*;

import static java.util.Comparator.comparing;

public class BiuroPodrozy {
    private String nazwa;

    private List<Wycieczka> ofertaWycieczek;


    public BiuroPodrozy(String nazwa) {
        this.nazwa = nazwa;
        ofertaWycieczek = new ArrayList<Wycieczka>();
    }


    public void dodajWycieczke(Wycieczka wycieczka) {
        ofertaWycieczek.add(wycieczka);
    }

    public void odwolajWycieczke(String nazwa) {
        boolean znaleziono = false;
        Iterator<Wycieczka> i = ofertaWycieczek.iterator();
        while (i.hasNext()) {
            Wycieczka w = i.next();
            if (nazwa.equals(w.getNazwaWycieczki())) {
                System.out.println("Odwołano poprawnie");
                znaleziono = true;
                i.remove();
            }
        }
        if (!znaleziono) {
            System.out.println("Nie znaleziono takiej wycieczki");
        }
    }

    public void wyswietlWycieczkiOdNajbliższej() {
        List<Wycieczka> tymczasowaDlaWycieczek = getOfertaWycieczekCopy();
        tymczasowaDlaWycieczek.sort(comparing(Wycieczka::getSortowalnyFormatDatyRozpoczecia));
        for (Wycieczka wycieczka : tymczasowaDlaWycieczek) {
            System.out.println(wycieczka.getDataRozpoczecia() + " " + wycieczka.getNazwaWycieczki());
            System.out.println("Ilosc dostepnych miejsc: " + (100 - wycieczka.getListaNormikow().size()) + " standardowych oraz " + (20 - wycieczka.getListaVipow().size() + " all inclusive"));

        }

    }

    public void wyswietlObjazdoweOdNajblizszej() {
        List<Wycieczka> tymczasowaDlaObjazdowej = new ArrayList<>();
        for (Wycieczka wycieczka : getOfertaWycieczek()) {
            if (wycieczka.getRodzajwycieczki() == RodzajWycieczki.OBJAZDOWA) {
                tymczasowaDlaObjazdowej.add(wycieczka);
            }
        }
        tymczasowaDlaObjazdowej.sort(comparing(Wycieczka::getSortowalnyFormatDatyRozpoczecia));
        System.out.println("Wycieczki objazdowe:");
        System.out.println("");
        for (Wycieczka wycieczka : tymczasowaDlaObjazdowej) {
            System.out.println(wycieczka.getDataRozpoczecia() + "" + wycieczka.getNazwaWycieczki());
            System.out.println("Ilosc dostepnych miejsc: " + (100 - wycieczka.getListaNormikow().size()) + " standardowych oraz " + (20 - wycieczka.getListaVipow().size() + " all inclusive"));

        }

    }

    public void wyswietlWypoczynkoweOdNajblizszej() {
        List<Wycieczka> tymczasowaDlaWypoczynkowej = new ArrayList<>();
        for (Wycieczka wycieczka : getOfertaWycieczek()) {
            if (wycieczka.getRodzajwycieczki() == RodzajWycieczki.WYPOCZYNKOWA) {
                tymczasowaDlaWypoczynkowej.add(wycieczka);
            }
        }
        tymczasowaDlaWypoczynkowej.sort(comparing(Wycieczka::getSortowalnyFormatDatyRozpoczecia));
        System.out.println("Wycieczki wypoczynkowe:");
        System.out.println("");
        for (Wycieczka wycieczka : tymczasowaDlaWypoczynkowej) {
            System.out.println(wycieczka.getDataRozpoczecia() + "" + wycieczka.getNazwaWycieczki());
            System.out.println("Ilosc dostepnych miejsc: " + (100 - wycieczka.getListaNormikow().size()) + " standardowych oraz " + (20 - wycieczka.getListaVipow().size() + " all inclusive"));


        }

    }

    public void wyswietlZwiedzanieOdNajblizszej() {
        List<Wycieczka> tymczasowaDlaZwiedzania = new ArrayList<>();
        for (Wycieczka wycieczka : getOfertaWycieczek()) {
            if (wycieczka.getRodzajwycieczki() == RodzajWycieczki.ZWIEDZANIE) {
                tymczasowaDlaZwiedzania.add(wycieczka);
            }
        }
        tymczasowaDlaZwiedzania.sort(comparing(Wycieczka::getSortowalnyFormatDatyRozpoczecia));
        System.out.println("Wycieczki ze zwiedzaniem:");
        System.out.println("");
        for (Wycieczka wycieczka : tymczasowaDlaZwiedzania) {
            System.out.println(wycieczka.getDataRozpoczecia() + "" + wycieczka.getNazwaWycieczki());
            System.out.println("Ilosc dostepnych miejsc: " + (100 - wycieczka.getListaNormikow().size()) + " standardowych oraz " + (20 - wycieczka.getListaVipow().size() + " all inclusive"));


        }

    }

    public void znajdzWycieczkeIPodajInformacje(String nazwa) {
        for (Wycieczka wycieczka : getOfertaWycieczek()) {
            if (nazwa.equals(wycieczka.getNazwaWycieczki())) {
                wycieczka.wypiszInfoOWycieczce();
                wycieczka.wypiszInfoOHotelu();
            }

        }

    }

    public void wypiszWycieczkiOdDnia(String data) {
        List<Wycieczka> tymczasowaOdDnia = getOfertaWycieczekCopy();
        String temp = data;
        String part1 = temp.substring(0, 2);
        String part2 = temp.substring(3, 5);
        String part3 = temp.substring(6, 10);
        String nowy = part3 + part2 + part1;
        int wlasciwaDataGraniczna = Integer.parseInt(nowy);
        Iterator<Wycieczka> i = ofertaWycieczek.iterator();
        while (i.hasNext()) {
            Wycieczka o = i.next();
            if (o.getSortowalnyFormatDatyRozpoczecia() < wlasciwaDataGraniczna) {
                i.remove();
            }
        }
        tymczasowaOdDnia.sort(comparing(Wycieczka::getSortowalnyFormatDatyRozpoczecia));
        for (Wycieczka wycieczka : tymczasowaOdDnia) {
            System.out.println(wycieczka.getDataRozpoczecia() + " " + wycieczka.getNazwaWycieczki());
            System.out.println("Ilosc dostepnych miejsc: " + (100 - wycieczka.getListaNormikow().size()) + " standardowych oraz " + (20 - wycieczka.getListaVipow().size() + " all inclusive"));
        }

    }

    public void wypiszWycieczkiWLokalizacji(String lokalizacja) {
        List<Wycieczka> tymczasowaDlaLokalizacji = new ArrayList<>();
        for (Wycieczka wycieczka : getOfertaWycieczek()) {
            if (lokalizacja.equals(wycieczka.getLokalizacja())) {
                tymczasowaDlaLokalizacji.add(wycieczka);
            }

        }
        tymczasowaDlaLokalizacji.sort(comparing(Wycieczka::getSortowalnyFormatDatyRozpoczecia));
        System.out.println("Lista wycieczek do " + lokalizacja);
        for (Wycieczka wycieczka : tymczasowaDlaLokalizacji) {
            wycieczka.wypiszInfoOWycieczce();
            wycieczka.wypiszInfoOHotelu();

        }
    }

    public List<Wycieczka> zwrocListeWycieczekPosortowanePoCenie() {
        List<Wycieczka> wycieczkiPoCenie = getOfertaWycieczekCopy();
        wycieczkiPoCenie.sort(comparing(Wycieczka::getCena));
        return wycieczkiPoCenie;
    }

    public List<Wycieczka> zwrocListeWycieczekPosortowanychAlfabetycznie() {
        List<Wycieczka> wycieczkiAlfabetycznie = getOfertaWycieczekCopy();
        wycieczkiAlfabetycznie.sort(comparing(Wycieczka::getNazwaWycieczki));
        return wycieczkiAlfabetycznie;
    }

    public List<Wycieczka> zwrocListeWycieczekTanszychNiz(List<Wycieczka> listaWycieczek, double zakresCeny) {
        List<Wycieczka> tanszeNizZakres = new ArrayList();
        for (Wycieczka wycieczka : listaWycieczek) {
            if (wycieczka.getCena() < zakresCeny) {
                tanszeNizZakres.add(wycieczka);
            }
        }
        return tanszeNizZakres;
    }

    public List<Wycieczka> getWycieczkiAlfabetycznie(List listaWycieczek) {
        List tymczasowa = new ArrayList<>(listaWycieczek);
        Collections.copy(tymczasowa, listaWycieczek);
        tymczasowa.sort(comparing(Wycieczka::getNazwaWycieczki));
        return tymczasowa;
    }

    public List<Wycieczka> getWycieczkiWypoczynkowe(List<Wycieczka> listaWycieczek) {
        List<Wycieczka> tymczasowa = new ArrayList<>();
        for (Wycieczka wycieczka : listaWycieczek) {
            if (wycieczka.getRodzajwycieczki() == RodzajWycieczki.WYPOCZYNKOWA) {
                tymczasowa.add(wycieczka);
            }
        }
        return tymczasowa;
    }

    public List<Wycieczka> getWycieczkiObjazdowe(List<Wycieczka> listaWycieczek) {
        List<Wycieczka> tymczasowa = new ArrayList<>();
        for (Wycieczka wycieczka : listaWycieczek) {
            if (wycieczka.getRodzajwycieczki() == RodzajWycieczki.OBJAZDOWA) {
                tymczasowa.add(wycieczka);
            }
        }
        return tymczasowa;
    }

    public List<Wycieczka> getWycieczkiZeZwiedzaniem(List<Wycieczka> listaWycieczek) {
        List<Wycieczka> tymczasowa = new ArrayList<>();
        for (Wycieczka wycieczka : listaWycieczek) {
            if (wycieczka.getRodzajwycieczki() == RodzajWycieczki.ZWIEDZANIE) {
                tymczasowa.add(wycieczka);
            }
        }
        return tymczasowa;
    }

    public void sortujWycieczkiPoRodzaju() {
        List<Wycieczka> tymczasowa = getOfertaWycieczekCopy();
        tymczasowa.sort(comparing(Wycieczka::getRodzajwycieczki));
        for (Wycieczka wycieczka : tymczasowa) {
            System.out.println(wycieczka.toString());

        }
    }

    public List<Wycieczka> getWycieczkiPoRodzaju(List<Wycieczka> listaWycieczek) {
        List tymczasowa = new ArrayList(listaWycieczek);
        Collections.copy(tymczasowa, listaWycieczek);
        tymczasowa.sort(comparing(Wycieczka::getRodzajwycieczki));
        return tymczasowa;
    }

    public List<Wycieczka> getWycieczkiWLokalizacji(List<Wycieczka> listaWycieczek, String nazwaLokalizacji) {
        boolean znaleziono = false;
        List<Wycieczka> tymczasowa = new ArrayList<>();
        for (Wycieczka wycieczka : listaWycieczek) {
            if (nazwaLokalizacji.equals(wycieczka.getLokalizacja())) {
                tymczasowa.add(wycieczka);
                znaleziono = true;
            }

        }
        if (znaleziono == false) {
            System.out.println("Nie znaleziono wycieczki o podanej lokalizacji");
        }
        return tymczasowa;
    }


    public void wyswietlWycieczkiWgLokalizacjiIInnegoKryterium() {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj lokalizacje");
        String input = in.nextLine();
        System.out.println("Jak sortować? Możliwości:");
        System.out.println("|cena|  |alfabetycznie| |rodzaj|");
        String input2 = in.nextLine();
        switch (input2) {
            case "alfabetycznie":
                System.out.println(getWycieczkiAlfabetycznie(getWycieczkiWLokalizacji(getOfertaWycieczek(), input)));
                break;
            case "cena":
                System.out.println("Podaj od ilu mają być tańsze oferty");
                double cena = in.nextDouble();
                System.out.println(zwrocListeWycieczekTanszychNiz(getWycieczkiWLokalizacji(getOfertaWycieczek(), input), cena));
                break;
            case "rodzaj":
                System.out.println("Jaki rodzaj wycieczki Cie interesuje?");
                System.out.println("|objazdowa|  |wypoczynkowa|  |zwiedzanie|");
                String rodzaj = in.nextLine();
                switch (rodzaj) {
                    case "objazdowa":
                        System.out.println(getWycieczkiObjazdowe(getWycieczkiWLokalizacji(getOfertaWycieczek(), input)));
                        break;
                    case "wypoczynkowa":
                        System.out.println(getWycieczkiWypoczynkowe(getWycieczkiWLokalizacji(getOfertaWycieczek(), input)));
                        break;
                    case "zwiedzanie":
                        System.out.println(getWycieczkiZeZwiedzaniem(getWycieczkiWLokalizacji(getOfertaWycieczek(), input)));
                        break;
                    default:
                        System.out.println("Wprowadzono niepoprawny rodzaj wycieczki!");

                }
                break;
            default:
                System.out.println("Wybrano niepoprawny rodzaj sortowania");


        }


    }


    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public List<Wycieczka> getOfertaWycieczek() {
        return ofertaWycieczek;
    }

    public List<Wycieczka> getOfertaWycieczekCopy() {
        List kopia = new ArrayList(ofertaWycieczek);
        Collections.copy(kopia, ofertaWycieczek);
        return kopia;
    }


    public void setOfertaWycieczek(List<Wycieczka> ofertaWycieczek) {
        this.ofertaWycieczek = ofertaWycieczek;
    }


}
