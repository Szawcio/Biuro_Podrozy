package Lukasz;


public class Klient {

    private String imie;
    private String nazwisko;
    private int pesel;
    private RodzajPlatnosci rodzajPlatnosci;
    private boolean czyMaUbezpieczenie;
    private RodzajKlienta rodzajKlienta;


    public Klient(String imie, String nazwisko, int pesel, RodzajPlatnosci rodzajPlatnosci, boolean czyMaUbezpieczenie, RodzajKlienta rodzajKlienta) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
        this.rodzajPlatnosci = rodzajPlatnosci;
        this.czyMaUbezpieczenie = czyMaUbezpieczenie;
        this.rodzajKlienta = rodzajKlienta;

    }

    public Klient() {
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getPesel() {
        return pesel;
    }

    public void setPesel(int pesel) {
        this.pesel = pesel;
    }

    public boolean isCzyMaUbezpieczenie() {
        return czyMaUbezpieczenie;
    }

    public void setCzyMaUbezpieczenie(boolean czyMaUbezpieczenie) {
        this.czyMaUbezpieczenie = czyMaUbezpieczenie;
    }

    public RodzajKlienta getRodzajKlienta() {
        return rodzajKlienta;
    }

    public void setRodzajKlienta(RodzajKlienta rodzajKlienta) {
        this.rodzajKlienta = rodzajKlienta;
    }

    public RodzajPlatnosci getRodzajPlatnosci() {
        return rodzajPlatnosci;
    }

    public void setRodzajPlatnosci(RodzajPlatnosci rodzajPlatnosci) {
        this.rodzajPlatnosci = rodzajPlatnosci;
    }

    @Override
    public String toString() {
        return "|Klient|  " +
                "Imie: " + imie +
                ", Nazwisko: " + nazwisko +
                ", Pesel: " + pesel +
                ", Metoda płatności: " + rodzajPlatnosci +
                ", Ubezpieczony: " + czyMaUbezpieczenie +
                ", Rodzaj Klienta: " + rodzajKlienta;
    }


}


