package Lukasz;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        Hotel h1 = new Hotel("kk", 3, true, "aa");
        Wycieczka w1 = new Wycieczka("dd", "kk", "12.05.2011", "kk", RodzajWycieczki.WYPOCZYNKOWA, h1,12);
        Wycieczka w2 = new Wycieczka("kk", "kk", "10.05.2011", "kk", RodzajWycieczki.ZWIEDZANIE, h1,10);
        Wycieczka w3 = new Wycieczka("zz", "kk", "12.01.2011", "kk", RodzajWycieczki.OBJAZDOWA, h1,9);
        Wycieczka w4 = new Wycieczka("aaa", "kk", "12.05.2014", "kk", RodzajWycieczki.WYPOCZYNKOWA, h1,7);
        Wycieczka w5 = new Wycieczka("bb", "kk", "12.11.2011", "kk", RodzajWycieczki.ZWIEDZANIE, h1,3);
        w2.generujListeNormikow();
        w2.generujListeVipow();
        w3.generujListeNormikow();
        w3.generujListeVipow();
        w4.generujListeNormikow();
        w4.generujListeVipow();
        w5.generujListeNormikow();
        w5.generujListeVipow();
        Klient klient = new Klient("lukasz", "michalak", 123456,RodzajPlatnosci.PRZELEWEM, true, RodzajKlienta.NORMIK);
        Klient klient2 = new Klient("lukasz", "kichalak", 123056, RodzajPlatnosci.PRZELEWEM, true, RodzajKlienta.NORMIK);
        Klient klient3 = new Klient("lukasz", "aichalak", 126456, RodzajPlatnosci.PRZELEWEM, true, RodzajKlienta.NORMIK);
        Klient klient4 = new Klient("lukasz", "zichalak", 136456, RodzajPlatnosci.PRZELEWEM, true, RodzajKlienta.NORMIK);
        Klient klient5 = new Klient("lukasz", "bichalak", 126496, RodzajPlatnosci.PRZELEWEM, true, RodzajKlienta.NORMIK);
        Klient klient6 = new Klient("lukasz", "zzz", 116496, RodzajPlatnosci.PRZELEWEM, true, RodzajKlienta.VIP);
        Klient klient7 = new Klient("lukasz", "bbb", 116490, RodzajPlatnosci.PRZELEWEM, true, RodzajKlienta.VIP);
        w1.generujListeNormikow();
        w1.generujListeVipow();
        w1.sprzedajWycieczke(klient);
        w1.sprzedajWycieczke(klient2);
        w1.sprzedajWycieczke(klient3);
        w1.sprzedajWycieczke(klient4);
        w1.sprzedajWycieczke(klient5);
        w1.sprzedajWycieczke(klient6);
        w1.sprzedajWycieczke(klient7);
        w1.ileWolnychMiejsc();
        w1.usunPoPeselu(123456);
        w1.ileWolnychMiejsc();
        w1.wyswietlPodroznychWgNazwiska();
        System.out.println("");
        w1.znajdzKlientaPoPeseluIWypiszDane(123056);
        w1.wypiszInfoOWycieczce();
        w1.wypiszInfoOHotelu();
        w1.getDataRozpoczecia();
        System.out.println("");
        String data = "12.03.2019";
        String liczba = data;
        String nowy1 = liczba.substring(0, 2);
        String nowy2 = liczba.substring(3, 5);
        String nowy3 = liczba.substring(6, 10);
        System.out.println(nowy1);
        System.out.println(nowy2);
        System.out.println(nowy3);
        String add = nowy3 + nowy2 + nowy1;
        System.out.println(add);
        System.out.println("");
        System.out.println("");
        System.out.println(w1.getSortowalnyFormatDatyRozpoczecia());
        System.out.println("");
        System.out.println("");
        BiuroPodrozy b1 = new BiuroPodrozy("123");
        b1.dodajWycieczke(w1);
        b1.dodajWycieczke(w2);
        b1.dodajWycieczke(w3);
        b1.dodajWycieczke(w4);
        b1.dodajWycieczke(w5);

        System.out.println(b1.getOfertaWycieczek());
        b1.getWycieczkiAlfabetycznie(b1.getOfertaWycieczek());
        System.out.println(b1.getOfertaWycieczek());


        System.out.println("");
        b1.sortujWycieczkiPoRodzaju();








    }
}
