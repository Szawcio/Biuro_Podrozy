package Lukasz;


public class Hotel {


    private String nazwa;
    private int iloscGwiazdek;
    private boolean maBasen;
    private String adres;

    public Hotel(String nazwa, int iloscGwiazdek, boolean maBasen, String adres) {
        this.nazwa = nazwa;
        this.iloscGwiazdek = iloscGwiazdek;
        this.maBasen = maBasen;
        this.adres = adres;
    }

    @Override
    public String toString() {
        return "|Hotel|  " +
                "Nazwa: " + nazwa +
                ", " + iloscGwiazdek + "-gwiazdkowy " +
                ", Czy dostępny jest basen: " + maBasen +
                ", Adres:" + adres;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getIloscGwiazdek() {
        return iloscGwiazdek;
    }

    public void setIloscGwiazdek(int iloscGwiazdek) {
        this.iloscGwiazdek = iloscGwiazdek;
    }

    public boolean isMaBasen() {
        return maBasen;
    }

    public void setMaBasen(boolean maBasen) {
        this.maBasen = maBasen;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }
}



