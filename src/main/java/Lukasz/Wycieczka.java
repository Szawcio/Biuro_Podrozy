package Lukasz;


import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

public class Wycieczka {

    private String nazwaWycieczki;

    private String lokalizacja;
    private String dataRozpoczecia;
    private String dataZakonczenia;
    private RodzajWycieczki rodzajwycieczki;
    private Hotel hotel;
    private double cena;

    private int sortowalnyFormatDatyRozpoczecia;


    private List<Klient> listaNormikow;
    private List<Klient> listaVipow;

    public Wycieczka(String nazwaWycieczki, String lokalizacja, String dataRozpoczecia, String dataZakonczenia, RodzajWycieczki rodzajwycieczki, Hotel hotel,double cena) {
        this.nazwaWycieczki = nazwaWycieczki;
        this.lokalizacja = lokalizacja;
        this.dataRozpoczecia = dataRozpoczecia;
        this.dataZakonczenia = dataZakonczenia;
        this.rodzajwycieczki = rodzajwycieczki;
        this.hotel = hotel;
        this.sortowalnyFormatDatyRozpoczecia = zwrocDateWFormacieInt();
        this.cena = cena;
    }

    public List<Klient> generujListeNormikow() {
        listaNormikow = new ArrayList<Klient>(100);
        return listaNormikow;
    }

    public List<Klient> generujListeVipow() {
        listaVipow = new ArrayList<Klient>(20);
        return listaVipow;
    }

    //"==" dziala lepiej do porownywania enumow niz ".equals()"
    public void sprzedajWycieczke(Klient klient) {
        if (klient.getRodzajKlienta() == RodzajKlienta.VIP) {
            if (20 - getListaVipow().size() > 0) {
                listaVipow.add(klient);
                System.out.println("Bilet na wycieczke, kupiony poprawnie");
            } else {
                System.out.println("Niestety nie ma już wolnego miejsca w ofercie all inclusive");
            }

        }
        if (klient.getRodzajKlienta() == RodzajKlienta.NORMIK) {
            if (100 - getListaNormikow().size() > 0) {
                listaNormikow.add(klient);
                System.out.println("Bilet na wycieczke, kupiony poprawnie");
            } else {
                System.out.println("Niestety nie ma już wolnego miejsca w ofercie all inclusive");
            }

        }
    }

    public void usunPoPeselu(int pesel) {
        boolean znaleziony = false;
        Iterator<Klient> i = listaNormikow.iterator();
        while (i.hasNext()) {
            Klient o = i.next();
            if (pesel == o.getPesel()) {
                System.out.println("Podrozny " + o.getImie() + " " + o.getNazwisko() + " został usunięty z listy podróżnych oferty standardowej");
                i.remove();
                znaleziony = true;
            }
        }
        Iterator<Klient> j = listaVipow.iterator();
        while (j.hasNext()) {
            Klient o = j.next();
            if (pesel == o.getPesel()) {
                System.out.println("Podrozny " + o.getImie() + " " + o.getNazwisko() + " został usunięty z listy podróżnych oferty all inclusive");
                j.remove();
                znaleziony = true;
            }

        }
        if (znaleziony == false) {
            System.out.println("Nie znaleziono podróżnego o podanym peselu");
        }


    }


    public void ileWolnychMiejsc() {
        if (100 - listaNormikow.size() > 0 || 20 - listaVipow.size() > 0) {
            System.out.println("Na wycieczke o nazwie: " + getNazwaWycieczki() + " pozostało:");
            System.out.println();
            System.out.println(100 - listaNormikow.size() + " wolnych miejsc w ofercie standardowej");
            System.out.println(20 - listaVipow.size() + " wolnych miejsc w ofercie all inclusive");
        } else {
            System.out.println("Na wycieczke o nazwie: " + getNazwaWycieczki() + " nie ma juz wolnych miejsc");
        }
    }

    public void wyswietlPodroznych() {
        System.out.println("Lista podróżnych wycieczki " + getNazwaWycieczki());
        System.out.println("Termin: " + getDataRozpoczecia() + "-" + getDataZakonczenia());
        System.out.println("Podróżni oferty standardowej:");
        System.out.println("");
        for (Klient normik : listaNormikow) {
            System.out.println(normik.getImie());


        }
    }

    public void wyswietlPodroznychWgNazwiska() {
        int nr;
        List<Klient> tymczasowaDlaNormikow = getListaNormikowCopy();
        tymczasowaDlaNormikow.sort(comparing(Klient::getNazwisko));
        List<Klient> tymczasowaDlaVipow = getListaVipowCopy();
        tymczasowaDlaVipow.sort(comparing(Klient::getNazwisko));

        System.out.println("Lista podróżnych wycieczki " + getNazwaWycieczki());
        System.out.println("Termin: " + getDataRozpoczecia() + "-" + getDataZakonczenia());
        System.out.println("Podróżni oferty standardowej:");
        System.out.println("");

        nr = 0;
        for (Klient normik : tymczasowaDlaNormikow) {
            nr++;
            System.out.println(nr + ". " + normik.getNazwisko() + " " + normik.getImie() + " " + normik.getPesel());
        }

        System.out.println("");
        System.out.println("Podróżni oferty all inclusive:");
        System.out.println("");

        nr = 0;
        for (Klient vip : tymczasowaDlaVipow) {
            nr++;
            System.out.println(nr + ". " + vip.getNazwisko() + " " + vip.getImie() + " " + vip.getPesel());
        }
    }

    public void zmienKlientaWVipaPoPeselu(int pesel) {
        boolean znaleziony = false;
        if (20 - listaVipow.size() > 0) {
            Iterator<Klient> i = listaNormikow.iterator();
            while (i.hasNext()) {
                Klient o = i.next();
                if (pesel == o.getPesel()) {
                    o.setRodzajKlienta(RodzajKlienta.VIP);
                    listaVipow.add(o);
                    i.remove();
                    znaleziony = true;
                }
            }
        } else {
            System.out.println("Brak miejsc na liście oferty all inclusive");
        }
        if (znaleziony == false) {
            System.out.println("Nie znaleziono podróżnego");
        }
    }

    public void znajdzKlientaPoPeseluIWypiszDane(int pesel) {

        Iterator<Klient> i = listaNormikow.iterator();
        boolean znaleziony = false;
        while (i.hasNext()) {
            Klient o = i.next();
            if (pesel == o.getPesel()) {
                znaleziony = true;
                System.out.println("Podróżny na liscie standardowej");
                System.out.println(o.toString());

            }
        }
        Iterator<Klient> j = listaVipow.iterator();
        while (j.hasNext()) {
            Klient o = j.next();
            if (pesel == o.getPesel()) {
                System.out.println("Podróżny na liście all inclusive");
                System.out.println(o.toString());
                znaleziony = true;
            }
        }
        if (znaleziony == false) {
            System.out.println("Nie znaleziono podróżnego");
        }
    }

    public void wypiszInfoOWycieczce() {
        System.out.println(this.toString());
    }

    public void wypiszInfoOHotelu() {
        System.out.println(this.getHotel().toString());
    }

    public int zwrocDateWFormacieInt() {
        String temp = this.dataRozpoczecia;
        String part1 = temp.substring(0, 2);
        String part2 = temp.substring(3, 5);
        String part3 = temp.substring(6, 10);
        String nowy = part3 + part2 + part1;
        int dataWlasciwegoFormatu = Integer.parseInt(nowy);
        return dataWlasciwegoFormatu;
    }

    public void wyswietlPlacacychPrzelewem(){
        System.out.println("Lista podroznych placacych przelewem: ");
        System.out.println("");
        System.out.println("Podrozni oferty standard:");
        for (Klient klient : listaNormikow) {
            if (klient.getRodzajPlatnosci()==RodzajPlatnosci.PRZELEWEM){
                System.out.println(klient.toString());
            }
        }
        System.out.println("Podrozni oferty all inclusive");
        for (Klient klient : listaVipow) {
            if (klient.getRodzajPlatnosci()==RodzajPlatnosci.PRZELEWEM){
                System.out.println(klient.toString());
            }

        }
    }

    public void wyswietlPlacacychGotowka(){
        System.out.println("Lista podroznych placacych gotowka: ");
        System.out.println("");
        System.out.println("Podrozni oferty standard:");
        for (Klient klient : listaNormikow) {
            if (klient.getRodzajPlatnosci()==RodzajPlatnosci.GOTOWKA){
                System.out.println(klient.toString());
            }
        }
        System.out.println("Podrozni oferty all inclusive");
        for (Klient klient : listaVipow) {
            if (klient.getRodzajPlatnosci()==RodzajPlatnosci.PRZELEWEM){
                System.out.println(klient.toString());
            }

        }
    }




    @Override
    public String toString() {
        return "|Wycieczka|  " +
                "Nazwa: " + nazwaWycieczki +
                ", Lokalizacja: " + lokalizacja +
                ", Data wyjazdu: " + dataRozpoczecia +
                ", Data powrotu: " + dataZakonczenia +
                ", Rodzaj wycieczki: " + rodzajwycieczki +
                ", Cena: " +cena+
                ", " + hotel.getIloscGwiazdek() + "-gwiazdowy" + "hotel " + hotel.getNazwa() +
                ", Wolnych miejsc w ofercie standardowej: " + (100 - listaNormikow.size()) +
                ", Wolnych miejsc all inclusive: " + (20 - listaVipow.size());

    }

    public String getNazwaWycieczki() {
        return nazwaWycieczki;
    }

    public void setNazwaWycieczki(String nazwaWycieczki) {
        this.nazwaWycieczki = nazwaWycieczki;
    }

    public String getLokalizacja() {
        return lokalizacja;
    }

    public void setLokalizacja(String lokalizacja) {
        this.lokalizacja = lokalizacja;
    }

    public String getDataRozpoczecia() {
        return dataRozpoczecia;
    }

    public void setDataRozpoczecia(String dataRozpoczecia) {
        this.dataRozpoczecia = dataRozpoczecia;
        this.sortowalnyFormatDatyRozpoczecia = zwrocDateWFormacieInt();
    }

    public String getDataZakonczenia() {
        return dataZakonczenia;
    }

    public void setDataZakonczenia(String dataZakonczenia) {
        this.dataZakonczenia = dataZakonczenia;
    }

    public RodzajWycieczki getRodzajwycieczki() {
        return rodzajwycieczki;
    }

    public void setRodzajwycieczki(RodzajWycieczki rodzajwycieczki) {
        this.rodzajwycieczki = rodzajwycieczki;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public List<Klient> getListaNormikow() {
        return listaNormikow;
    }

    public void setListaNormikow(List<Klient> listaNormikow) {
        this.listaNormikow = listaNormikow;
    }

    public List<Klient> getListaVipow() {
        return listaVipow;
    }

    public void setListaVipow(List<Klient> listaVipow) {
        this.listaVipow = listaVipow;
    }

    public int getSortowalnyFormatDatyRozpoczecia() {
        return sortowalnyFormatDatyRozpoczecia;
    }

    public void setSortowalnyFormatDatyRozpoczecia(int sortowalnyFormatDatyRozpoczecia) {
        this.sortowalnyFormatDatyRozpoczecia = sortowalnyFormatDatyRozpoczecia;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public List<Klient> getListaNormikowCopy(){
        List kopia = new ArrayList(listaNormikow);
        Collections.copy(kopia,listaNormikow);
        return kopia;
    }

    public List<Klient> getListaVipowCopy(){
        List kopia = new ArrayList(listaVipow);
        Collections.copy(kopia,listaVipow);
        return kopia;
    }
}
