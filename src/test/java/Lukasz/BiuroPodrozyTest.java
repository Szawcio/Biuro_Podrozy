package Lukasz;


import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertTrue;

public class BiuroPodrozyTest {

    BiuroPodrozy biuroPodrozy;

    @Before
    public void setup() {
        biuroPodrozy = new BiuroPodrozy("Podróże za jeden uśmiech");
    }

    @Test
    public void testDodajWycieczke() {
        Hotel hotel = new Hotel("ElMatadore", 2, false, "xxxxx");
        Wycieczka wycieczka = new Wycieczka("Dwutygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 12);
        biuroPodrozy.dodajWycieczke(wycieczka);
        boolean result = biuroPodrozy.getOfertaWycieczek().contains(wycieczka);
        assertTrue(result);


    }

    @Test
    public void testOdwolajWycieczke() {
        Hotel hotel = new Hotel("ElMatadore", 2, false, "xxxxx");
        Wycieczka wycieczka = new Wycieczka("Dwutygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 12);
        biuroPodrozy.dodajWycieczke(wycieczka);
        biuroPodrozy.odwolajWycieczke("Dwutygodniowy pobyt w Hiszpanii turnus:001");
        boolean result = biuroPodrozy.getOfertaWycieczek().contains(wycieczka);
        assertThat(result).isEqualTo(false);

    }

    @Test
    public void testZwrocListeWycieczekTanszychNiz() {
        Hotel hotel = new Hotel("ElMatadore", 2, false, "xxxxx");

        Wycieczka wycieczka1 = new Wycieczka("Dwutygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 1000);
        Wycieczka wycieczka2 = new Wycieczka("Dwutygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 1500);
        Wycieczka wycieczka3 = new Wycieczka("Dwutygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 1700);

        biuroPodrozy.getOfertaWycieczek().add(wycieczka1);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka2);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka3);

        int result = biuroPodrozy.zwrocListeWycieczekTanszychNiz(biuroPodrozy.getOfertaWycieczek(),1600).size();

        assertThat(result).isEqualTo(2);


    }

    @Test
    public void testGetWycieczkiAlfabetycznie() {
        Hotel hotel = new Hotel("ElMatadore", 2, false, "xxxxx");

        Wycieczka wycieczka1 = new Wycieczka("Tygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 1000);
        Wycieczka wycieczka2 = new Wycieczka("Dwutygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 1500);
        Wycieczka wycieczka3 = new Wycieczka("Czterowniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 1700);

        biuroPodrozy.getOfertaWycieczek().add(wycieczka1);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka2);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka3);

        Wycieczka result = biuroPodrozy.getWycieczkiAlfabetycznie(biuroPodrozy.getOfertaWycieczek()).get(0);

        assertThat(result.getNazwaWycieczki()).isEqualTo("Czterowniowy pobyt w Hiszpanii turnus:001");

    }

    @Test
    public void testGetWycieczkiWypoczynkowe() {
        Hotel hotel = new Hotel("ElMatadore", 2, false, "xxxxx");
        Wycieczka wycieczka1 = new Wycieczka("Tygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 1000);
        Wycieczka wycieczka2 = new Wycieczka("Dwutygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.WYPOCZYNKOWA, hotel, 1500);
        Wycieczka wycieczka3 = new Wycieczka("Czterowniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 1700);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka1);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka2);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka3);
        int result = biuroPodrozy.getWycieczkiWypoczynkowe(biuroPodrozy.getOfertaWycieczek()).size();
        assertThat(result).isEqualTo(1);

    }

    @Test
    public void testGetWycieczkiObjazdowe() {
        Hotel hotel = new Hotel("ElMatadore", 2, false, "xxxxx");
        Wycieczka wycieczka1 = new Wycieczka("Tygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.OBJAZDOWA, hotel, 1000);
        Wycieczka wycieczka2 = new Wycieczka("Dwutygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.WYPOCZYNKOWA, hotel, 1500);
        Wycieczka wycieczka3 = new Wycieczka("Czterowniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.OBJAZDOWA, hotel, 1700);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka1);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka2);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka3);
        int result = biuroPodrozy.getWycieczkiObjazdowe(biuroPodrozy.getOfertaWycieczek()).size();
        assertThat(result).isEqualTo(2);

    }

    @Test
    public void testGetWycieczkiZeZwiedzaniem() {
        Hotel hotel = new Hotel("ElMatadore", 2, false, "xxxxx");
        Wycieczka wycieczka1 = new Wycieczka("Tygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 1000);
        Wycieczka wycieczka2 = new Wycieczka("Dwutygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.WYPOCZYNKOWA, hotel, 1500);
        Wycieczka wycieczka3 = new Wycieczka("Czterowniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 1700);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka1);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka2);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka3);
        int result = biuroPodrozy.getWycieczkiZeZwiedzaniem(biuroPodrozy.getOfertaWycieczek()).size();
        assertThat(result).isEqualTo(2);

    }

    @Test
    public void testGetWycieczkiPoRodzaju() {
        Hotel hotel = new Hotel("ElMatadore", 2, false, "xxxxx");
        Wycieczka wycieczka1 = new Wycieczka("Tygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 1000);
        Wycieczka wycieczka2 = new Wycieczka("Dwutygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.WYPOCZYNKOWA, hotel, 1500);
        Wycieczka wycieczka3 = new Wycieczka("Czterowniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 1700);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka1);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka2);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka3);

        Wycieczka result = biuroPodrozy.getWycieczkiPoRodzaju(biuroPodrozy.getOfertaWycieczek()).get(0);

        assertThat(result.getRodzajwycieczki()).isEqualTo(RodzajWycieczki.WYPOCZYNKOWA);

    }

    @Test
    public void getWycieczkiWLokalizacji() {
        Hotel hotel = new Hotel("ElMatadore", 2, false, "xxxxx");
        Wycieczka wycieczka1 = new Wycieczka("Tygodniowy pobyt w Hiszpanii turnus:001", "Madryt", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 1000);
        Wycieczka wycieczka2 = new Wycieczka("Dwutygodniowy pobyt w Hiszpanii turnus:001", "mmmm", "24.07.2017", "07.07.2017", RodzajWycieczki.WYPOCZYNKOWA, hotel, 1500);
        Wycieczka wycieczka3 = new Wycieczka("Czterowniowy pobyt w Hiszpanii turnus:001", "mmmm", "24.07.2017", "07.07.2017", RodzajWycieczki.ZWIEDZANIE, hotel, 1700);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka1);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka2);
        biuroPodrozy.getOfertaWycieczek().add(wycieczka3);

        int result = biuroPodrozy.getWycieczkiWLokalizacji(biuroPodrozy.getOfertaWycieczek(), "Madryt").size();

        assertThat(result).isEqualTo(1);
    }


}
