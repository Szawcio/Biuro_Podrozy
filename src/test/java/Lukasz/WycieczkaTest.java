package Lukasz;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;

@RunWith(MockitoJUnitRunner.class)
public class WycieczkaTest {

    private Wycieczka wycieczka;

    @Before
    public void setup() {
        Hotel hotel = new Hotel("Przy plazy", 3, false, "wypoczynkowa 3a");
        wycieczka = new Wycieczka("Nad morze", "Dziwnow", "21.07.2017", "01.08.2017", RodzajWycieczki.WYPOCZYNKOWA, hotel, 12);
    }

    @Test
    public void testGenerujNormiki() {
        wycieczka.generujListeNormikow();
        boolean resultListaNormikow = (wycieczka.getListaNormikow().size() == 0);
        assertThat(resultListaNormikow).isEqualTo(true);


    }

    @Test
    public void testGenerujVipy() {
        wycieczka.generujListeVipow();
        boolean resultListaVipow = (wycieczka.getListaVipow().size() == 0);
        assertThat(resultListaVipow).isEqualTo(true);

    }

    @Test
    public void testSprzedajWycieczkeKlientowiNormikowi() {
        wycieczka.generujListeNormikow();
        Klient klient = spy(Klient.class);
        klient.setRodzajKlienta(RodzajKlienta.NORMIK);
        wycieczka.sprzedajWycieczke(klient);
        boolean result = wycieczka.getListaNormikow().contains(klient);
        assertThat(result).isEqualTo(true);
    }

    @Test
    public void testSprzedajWycieczkeKlientowiVipowi() {
        wycieczka.generujListeVipow();
        Klient klient = new Klient("lukasz", "michalak", 123456, RodzajPlatnosci.PRZELEWEM, true, RodzajKlienta.VIP);
        wycieczka.sprzedajWycieczke(klient);
        boolean result = wycieczka.getListaVipow().contains(klient);
        assertThat(result).isEqualTo(true);
    }

    @Test
    public void testUsunPodroznegoNormikaPoPeselu() {
        wycieczka.generujListeNormikow();
        wycieczka.generujListeVipow();
        Klient klient = spy(Klient.class);

        klient.setRodzajKlienta(RodzajKlienta.NORMIK);
        klient.setPesel(112);
        klient.setImie("Jan");
        klient.setNazwisko("Kowalski");

        wycieczka.sprzedajWycieczke(klient);
        wycieczka.usunPoPeselu(112);
        boolean result = wycieczka.getListaNormikow().contains(klient);
        assertThat(result).isEqualTo(false);


    }

    @Test
    public void testUsunPodroznegoVipaPoPeselu() {
        wycieczka.generujListeNormikow();
        wycieczka.generujListeVipow();
        Klient klient = spy(Klient.class);

        klient.setRodzajKlienta(RodzajKlienta.VIP);
        klient.setPesel(234);
        klient.setImie("Marek");
        klient.setNazwisko("Nowak");

        wycieczka.sprzedajWycieczke(klient);
        wycieczka.usunPoPeselu(234);
        boolean result = !wycieczka.getListaVipow().contains(klient);
        assertTrue(result);

    }

    @Test
    public void testZmienKlientaWVipaPoPeselu() {
        wycieczka.generujListeNormikow();
        wycieczka.generujListeVipow();
        Klient klient = spy(Klient.class);

        klient.setRodzajKlienta(RodzajKlienta.NORMIK);
        klient.setPesel(234);
        klient.setImie("Marek");
        klient.setNazwisko("Nowak");

        wycieczka.sprzedajWycieczke(klient);
        wycieczka.zmienKlientaWVipaPoPeselu(234);

        RodzajKlienta poZmianie = klient.getRodzajKlienta();

        assertThat(poZmianie).isEqualTo(RodzajKlienta.VIP);

    }

    @Test
    public void testUstawFormatDatyRozpoczecia() {
        wycieczka.zwrocDateWFormacieInt();

        int result = wycieczka.getSortowalnyFormatDatyRozpoczecia();
        assertThat(result).isEqualTo(20170721);

    }


}

